import sys
import os
import datetime

def fix_descrypt(line):
	# separate hash:pw into components 
	hash_ = line.split(":")[0]
	pw = line.split(":")[1]

	# if pw is 8 chars, remove last char
	if len(pw) == 9:
		pw = pw[:7] + os.linesep

	# return reconstructed hash:pw
	return str(hash_ + ":" + pw)

# not much explanation needed
# add each line of file to a list, checking if line already exists in list before adding to new file
def remove_dupes(broken_filename):

	new_filename = "fixed.broken"
	file = open(broken_filename, "r").readlines()
	file.sort()

	new_list = []
	new_file = open(new_filename, "w")

	for line in file:
		if line not in new_list:
			new_list.append(line)

	for line in new_list:
		new_file.write(line)

	# this is crap, i know
	new_file.close()
	os.remove(broken_filename)
	os.rename(new_filename, broken_filename)


def remove_semi(line):
	return line.replace(":", " ")

def main():

	BROKEN_FILENAME = "output.broken"

	# one argument: name of potfile
	pot_filename = sys.argv[1]

	# if already a .broken by above name, rename existing one
	if BROKEN_FILENAME in os.listdir():
		os.rename(BROKEN_FILENAME, str(datetime.datetime.now()) + ".broken")

	final_file = open(BROKEN_FILENAME, "w")
	
	file = open(pot_filename, "r")
	for line in file:
		# if current line is not a descrypt hash
		if line[0] == "$":
			final_file.write(remove_semi(line))
		else:
			final_file.write(remove_semi(fix_descrypt(line)))

	final_file.close()

	remove_dupes(BROKEN_FILENAME)

main()
