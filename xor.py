import os, sys
import secretsharing as sss
import jsonpickle
from hashlib import sha256
from passlib.hash import pbkdf2_sha256,argon2,sha512_crypt,sha1_crypt
from random import randrange
import base64
from Crypto.Cipher import AES
from Crypto import Random

def pxor(pwd,share):

    '''
      XOR a hashed password into a Shamir-share

      1st few chars of share are index, then "-" then hexdigits
      we'll return the same index, then "-" then xor(hexdigits, sha256(pwd))
      we truncate the sha256(pwd) to if the hexdigits are shorter
      we left pad the sha256(pwd) with zeros if the hexdigits are longer
      we left pad the output with zeros to the full length we xor'd
    '''

    # split starting (non xor) share into index-
    words = share.split("-")

    hexshare = words[1]

    slen = len(hexshare)

    hashpwd = sha256(pwd).hexdigest()

    hlen = len(hashpwd)

    outlen = 0

    #prep for truncating or padding the sha256
    if slen < hlen:
        outlen = slen
        hashpwd = hashpwd[0:outlen]
    elif slen > hlen:
        outlen = slen
        hashpwd = hashpwd.zfill(outlen)
    else:
        outlen = hlen

    xorvalue = int(hexshare, 16) ^ int(hashpwd, 16) # convert to integers and xor 
    paddedresult = '{:x}'.format(xorvalue)          # convert integer xor result back to hex
    paddedresult = paddedresult.zfill(outlen)       # pad left
    result = words[0] + "-" + paddedresult          # put index back
    return result


def newsecret(numbytes):
    '''
        let's get a number of pseudo-random bytes, as a hex string
    '''
    binsecret = open("/dev/urandom", "rb").read(numbytes)
    secret = binsecret.encode('hex')
    return secret

def pwds_shares_to_secret(kpwds, kinds, diffs):
    '''
        take k passwords, indices of those, and the "public" shares and 
        recover shamir secret
    '''
    shares = []
    for i in range (0, len(kpwds)):
        shares.append(pxor(kpwds[i], diffs[kinds[i]]))
    secret = sss.SecretSharer.recover_secret(shares)
    return secret

def pwds_to_shares(pwds,k,numbytes):
    '''
        Give a set of n passwords, and a threshold (k) generate a set
        of Shamir-like 'public' shares for those.

        We do this by picking a random secret, generating a set of
        Shamir-shares for that, then XORing a hashed password with 
        each share.  Given the set of 'public' shares and k of the
        passwords, one can re-construct the secret.

        Note:  **There are no security guarantees for this**
        This is just done for a student programming exercise, and
        is not for real use. With guessable passwords, the secret 
        can be re-constructed!

    '''
    n = len(pwds) # we're in k-of-n mode...
    secret = newsecret(numbytes) # generate random secret
    print_secret(secret)


    #  generating shares pre-xor: this is what we need to regenerate the secret, I think
    shares = sss.SecretSharer.split_secret(secret, k, n) # split secret -- n is number of passwords, k is decided threshold
    
    '''
    	these are the shares the library generates
    	i.e. the shares that will unlock the secret
    	the ones in our infernoball are these combined with our passwords
    '''
    print ("Our generated shares: ") 
    for share in shares:
    	print (share)
    print ("")

    diffs = [] # diff the passwords and shares
    

    for i in range(0, n):

    
        diffs.append(pxor(pwds[i], shares[i]))
    	

    #return diffs
    return [diffs, shares]

def print_secret(secret):
	print ("Our generated secret: " + secret)
	print ("")


def main():

	'''
	i.		grab a few easy example passwords
	ii. 	use his code to generate 3 shares with k=2, so we need 2-of-3 shares 
	iii.	for purpose of understanding - get the sha256 of these passwords,
				note that it's a substring of sha256(password) that is xor'd 
				with the shares the secretsharing library generates - the result
				is the shares we are given
	iv.		using Stephen's code to regenerate the secret we started with
				- successfully. yay. 

	still not *fully* sure how his recover_secret code works
	also need to figure out what to do with the secret once we get it

	'''

	BLOCK_SIZE = 8 # crypto purposes - his default is 16

	test_passwords = ["password", "passw0rd", "Password"] # n = 3

	print ("Our test passwords: ")
	for pw in test_passwords:
		print pw 
	print("")

	print ("sha256 of test passwords:")
	for pw in test_passwords:
		print(sha256(pw).hexdigest())
	print("")

	test_k_threshold = 2 # think this is 2, so k-of-n = 2-of-3, so 2 shares required...

	shares_and_diffs = pwds_to_shares(test_passwords, test_k_threshold, BLOCK_SIZE)
	diffs = shares_and_diffs[0]
	shares = shares_and_diffs[1]

	# i.e. the shares we'll be given, we will effectively have to work backwards...
	print("These are our shares XOR'd with their respective passwords...") 
	for item in diffs:
		print (str(item))
	print("")

	'''
		attempting to reconstruct the secret 
		imagine we recovered the passwords we generated above from hashes...
		testing different scenarios below
	'''
	# TEST 0 - first 2 passwords,
	#recovered_passwords = test_passwords[:2] 	
	#indices = [0,1] # store the indexes of our recovered passwords 
	# recovered_diffs = diffs[:2]

	# TEST 1 - first and third password
	recovered_passwords = [test_passwords[0], test_passwords[2]]
	#indeces = [0,2] did not work as expected, went out of bounds
	indices = [0,1]	
	recovered_diffs = [diffs[0], diffs[2]]

	# TEST 2
	recovered_passwords = test_passwords
	indices = [0,1,2] # don't see the point of this....so sensitive????
	recovered_diffs = diffs

	'''
		might do this manually instead of using his method tbh
	'''
	#recovered_secret = sss.SecretSharer.recover_secret(shares)
	recovered_secret = pwds_shares_to_secret(recovered_passwords, indices, recovered_diffs)


	#print("length of shares list = " + str(len(shares)))

	print("recovered secret: " + recovered_secret)
	



main()
